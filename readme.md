## CSS Slider for Magento1.9 

### Autoplay + on click shift with CSS only

Modified for 4 Slides and for use with Magento. 

#### How to use:

* phtml file goes to your templates. In mage 1.9: `/html/magento/app/design/frontend/BRAND/THEME/template/CUSTOM`
* css to skin, wherever the rest of your CSS lives

The slides are static blocks identified as `homeslider1` -> `homeslider4`, see the phtml.

There is an option to have the title of the block as a text inside the slider, the title is currently disabled via CSS only (display: none) and can be easily recovered.

The full slider has to be called into your CMS page or template as a regular custom static block. In Mage 1.9 in a CMS page:
`{{block type="core/template" template="CUSTOM/slider-home.phtml"}}`
